import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.6.20"
    id("org.jetbrains.kotlin.plugin.allopen") version "1.6.20"
    application
}

group = "tech.materialize"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.6.20")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.0")
    implementation("io.mockk:mockk:1.12.2")
    implementation("net.bytebuddy:byte-buddy:1.12.8")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
        languageVersion = "1.7"
    }
}

application {
    mainClass.set("tech.materialize.data.MainKt")
}

allOpen {
    annotations("tech.materialize.data.Copyable")
}