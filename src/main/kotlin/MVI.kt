package tech.materialize.data

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

data class Model(
    val message: String,
    val dialog: Dialog?
) {
    data class Dialog(
        val message: String
    )
}

interface Controller {
    fun message(message: String)
}

class ViewModelNoFlow(
    private val present: (Model) -> Unit
) : Controller {

    private var message: String = ""

    private fun model() = Model(
        message = message,
        dialog = null
    )

    override fun message(message: String) {
        this.message = message
        present(model())
    }
}

class ViewModelFlow(
    private val present: (Model) -> Unit,
    scope: CoroutineScope
) : Controller {

    init {
        scope.launch {
            state.collect(present)
        }
    }

    private val state = MutableStateFlow(Model(
        message = "",
        dialog = null
    ))

    override fun message(message: String) {
        state.update { it.copy(message = message) }
    }
}

private val state = MutableStateFlow<Model>(
    Model(
        message = "Hello World!",
        dialog = null
    )
)

fun main() {
    GlobalScope.launch {
        state.collect {

        }
    }
}