package tech.materialize.data

annotation class Copyable

@Copyable
data class MyData(val order: Nested) {
    @Copyable
    data class Nested(
        val catchphrase: String,
        val customer: Customer
    ) {
        @Copyable
        data class Customer(
            val name: Name,
            val billing: Billing
        ) {
            @Copyable
            data class Name(
                val lastName: String,
                val firstName: String,
                val number: Int
            )
            @Copyable
            data class Billing(
                val address: Address
            ) {
                @Copyable

                data class Address(
                    val streetNumber: String
                )
            }
        }
    }
}