package tech.materialize.data

import io.mockk.every
import io.mockk.spyk
import kotlinx.coroutines.flow.MutableStateFlow
import net.bytebuddy.ByteBuddy
import net.bytebuddy.description.modifier.Visibility
import net.bytebuddy.dynamic.DynamicType
import net.bytebuddy.dynamic.scaffold.subclass.ConstructorStrategy
import net.bytebuddy.implementation.FieldAccessor
import net.bytebuddy.implementation.Implementation
import net.bytebuddy.implementation.MethodCall
import net.bytebuddy.implementation.bytecode.member.MethodInvocation
import java.lang.reflect.Constructor
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KProperty0
import kotlin.reflect.full.functions
import kotlin.reflect.jvm.javaField

private val state = MutableStateFlow<MyData?>(null)

typealias Publish<T> = (T.() -> Copy<T>) -> Unit

class aViewModel(
    private val publish: Publish<MyData.Nested.Customer.Name>
) {
    fun lastName(lastName: String) {
        // validate
        publish {
            this::lastName set lastName
        }
    }
}

private fun <T> DynamicType.Builder<T>.defineFields(constructor: Constructor<*>): DynamicType.Builder<T> {
    var accumulator = this
    for (parameter in constructor.parameters) {
        accumulator = accumulator.defineField(parameter.name, parameter.type, Visibility.PRIVATE)
    }
    return accumulator
}

private fun Implementation.Composable.assignFieldsToArguments(constructor: Constructor<*>): Implementation.Composable {
    var accumulator = this
    for ((index, parameter) in constructor.parameters.withIndex()) {
        accumulator = accumulator.andThen(FieldAccessor.ofField(parameter.name).setsArgumentAt(index))
    }
    return accumulator
}

class ShadowFactory {
    private var shadowProxies = mutableMapOf<Class<*>, Class<*>>()

    private fun <T> getShadowProxyClass(clazz: Class<T>): Class<out T> {
        return shadowProxies.getOrPut(clazz) {
            println("making $clazz shadow")
            val constructor = clazz.declaredConstructors.first()
            val arguments = constructor.parameters.map { getShadowProxy(it.type) }.toTypedArray()
            return ByteBuddy()
                .subclass(clazz, ConstructorStrategy.Default.NO_CONSTRUCTORS)
                .name("${clazz.simpleName}.Copy")
                .defineConstructor(Visibility.PUBLIC).intercept(
                    MethodCall.invoke(constructor)
                        .with(*arguments)
                )
                .make()
                .load(clazz.classLoader)
                .loaded
        } as Class<T>
    }

    fun <T> getShadowProxy(clazz: Class<T>): T = when(clazz) {
        String::class.java -> ByteBuddy().subclass(clazz).name("newname").make().load(clazz.classLoader).loaded.newInstance() as T
        Int::class.java -> 0 as T
        else -> getShadowProxyClass(clazz).getDeclaredConstructor().newInstance()
    }
}

fun main() {
    val data = MyData(
        MyData.Nested(
            catchphrase = "",
            customer = MyData.Nested.Customer(
                name = MyData.Nested.Customer.Name(
                    lastName = "",
                    firstName = "",
                    number = 0
                ),
                billing = MyData.Nested.Customer.Billing(
                    address = MyData.Nested.Customer.Billing.Address(
                        streetNumber = ""
                    )
                )
            )
        )
    )

    val delegateClass = ShadowFactory().getShadowProxy(MyData.Nested.Customer.Billing.Address::class.java)

    val string = spyk(objToCopy = "") {
        every { this@spyk.length } returns 20
    }
    val length = string.length

    val brandNew = data.copyX {
        ::order set {
            ::customer set {
                ::name set {
                    ::firstName set "joe"
                }
            }
        }
    }

    val uiState = data.copy(
        order = data.order.copy(
            catchphrase = "what is now to come"
        )
    )

    val newWay = uiState.copy { ::order } copy { ::catchphrase } assign "what is now to come"
//    val newWayZ = oldWay[{ ::nested }][{ ::catchphrase }].assign("what is now to come")

    val newWay1 = uiState.copyAll {
        copy { ::order } copy { ::customer } copy { ::name } copy { ::firstName } set "tom"
        copy { ::order } copy { ::customer } copy { ::name } copy { ::lastName } set "cruise"
    }
//    val newWay2 = uiState.copy {
//        [{::order}][{::customer}][{ ::firstName }] = "tom"
//        [{::order}][{::customer}][{ ::lastName }] = "cruise"
//    }

    val newUiState = uiState
        .copy { ::order }
        .copy { ::customer }
        .copyAll {
            copy { ::name } copyAll {
                copy { ::firstName } set "james"
                copy { ::lastName } set "bond"
            }
            copy { ::billing }.copy { ::address }.copyAll {
                copy { ::streetNumber } set "007"
            }
        }

    val test = uiState.copy { ::order }::assign

//    val newUIState = uiState.update {
//        ::order.update {
//            ::customer.update {
//                ::name.update "bob"
//            }
//        }
//    }
//

//    uiState.copy(
//        order = uiState.order.copy(
//            customer = uiState.order.customer.copy(
//                firstName = "bob"
//            )
//        )
//    )
//
//    uiState.update()
//        .copy { ::order }
//        .copy { ::customer }
//        .copyAll {
//            copy { ::name } copyAll {
//                copy { ::firstName } set "james"
//                copy { ::lastName } set "bond"
//            }
//            copy { ::billing }.copy { ::address }.copyAll {
//                copy { ::streetNumber } set "007"
//            }
//        }
//
//    val kotlin4 = uiState
//        ::nested()
//        ::customer()
//        .apply {
//            ::name().apply {
//                ::firstName modify "james"
//                ::lastName modify "bond"
//            }
//            ::billing::address.apply {
//                ::streetNumber modify "007"
//            }
//        }

    val oldWay3 = uiState.copy(
        order = uiState.order.copy(
            customer = uiState.order.customer.copy(
                name = uiState.order.customer.name.copy(
                    firstName = "james",
                    lastName = "bond"
                ),
                billing = uiState.order.customer.billing.copy(
                    address = uiState.order.customer.billing.address.copy(
                        streetNumber = "007"
                    )
                )
            )
        )
    )

//    val newWay4 = MyData::class
//        .atc { ::order }
//        .copy { ::customer }
//        .copyAll {
//            copy { ::name }.copyAll {
//                copy { ::firstName } set "james"
//                copy { ::lastName } set "bond"
//            }
//            copy { ::billing }.copy { ::address }.copyAll {
//                copy { ::streetNumber } set "007"
//            }
//        }

    """
        {
            "nested.customer" : {
                "name" : {
                    "firstName" : "james",
                    "lastName" : "bond"
                },
                "billing.address" : {
                    "streetNumber" : "007"
                }
            }
        }
    """.trimIndent()

//    val newWay5 = MyData::class
//        .atc { ::order }
//        .copy { ::customer }
//        .copy { ::name }
//        .copy { ::firstName }
//        .copyAll { it.uppercase() }

//    """
//        {
//            "nested.customer.name.firstName" : "MainKt$lambda$0"
//        }
//    """.trimIndent()
//    """
//        {
//            "nested.customer.name.firstName" : "it.capitalize()"
//        }
//    """.trimIndent()
//    """
//        {
//            "nested.customer.name.firstName" : "Main.kt$76:15-76:30"
//        }
//    """.trimIndent()

    println(newWay)



}

inline fun <reified Root : Any> Root.copyX(noinline block: Root.() -> Copy<Root>): Root = copyX(Root::class, block)

fun <T : Any> T.copyX(valueClass: KClass<T>, copyBuilder: T.() -> Copy<T>): T = copyBuilder().copy(this, valueClass)

//fun <Next : Any, Root : Any> KClass<Root>.patch(block: Root.() -> Copy<Next, Root>): Copy<Next, Root> {
//    TODO()
//}
//fun <Next : Any, Root : Any> Root.copyFrom(kClass: KClass<Root>, patch: Copy<Next, Root>): Root = block().copy(this, kClass)

inline infix fun <reified Next : Any, Previous : Any> KProperty0<Next>.set(noinline block: Next.() -> Copy<Next>) =
    Builder<Next, Previous>(this, Next::class, block)
//
//class BundleOfJoy<T : Any>(val property: KProperty0<T>)
//
//fun <Next : Any> KProperty0<Next>.set() = BundleOfJoy(this)

//inline infix fun <reified Next : Any, Previous : Any> BundleOfJoy<Next>.set(noinline block: Next.() -> Copy<Next>) =
//    Builder<Next, Previous>(property, Next::class, block)

//fun <Previous : Any, Root : Any> Previous.all(vararg other: Copy<Previous, Root>): Copy<Previous, Root> {
//    return other.first()
//}

//infix fun <Next : Any, Previous : Any> BundleOfJoy<Next>.value(nextValue: Next) = Value<Next, Previous>(property, nextValue)

infix fun <Next : Any, Previous : Any> KProperty0<Next>.set(nextValue: Next) = Value<Next, Previous>(this, nextValue)
//infix fun <Next : Any, Previous : Any> BundleOfJoy<Next>.set(nextValue: Next) = Value<Next, Previous>(property, nextValue)


interface Copy<T : Any> {
    fun copy(value: T, valueClass: KClass<T>): T
}

class Value<in Next : Any, Previous : Any>(
    private val property: KProperty0<Next>,
    private val next: Next
) : Copy<Previous> {
    init {
        val javaField = property.javaField
        requireNotNull(javaField) { "$property cannot be modified via copy function" }
        require(javaField.type.isAssignableFrom(next::class.java)) { "$property is not assignable from $next" }
    }

    override fun copy(value: Previous, valueClass: KClass<Previous>): Previous = value.copy(valueClass, property, next)
}

class Builder<in Next : Any, Previous : Any>(
    private val property: KProperty0<Next>,
    private val nextClass: KClass<Next>,
    private val nextCopyBuilder: Next.() -> Copy<Next>
) : Copy<Previous> {
    override fun copy(value: Previous, valueClass: KClass<Previous>): Previous =
        value.copy(
            valueClass,
            property,
            property.get().let { next ->
                next.copyX(nextClass) {
                    nextCopyBuilder(next)
                }
            }
        )
}

typealias NextProperty<Previous, Next> = Previous.() -> KProperty0<Next>

@Suppress("SuspiciousCallableReferenceInLambda")
inline fun <reified Root : Any, reified Next : Any> Root.copy(
    noinline nextProperty: NextProperty<Root, Next>
): CopyChain<Next, Root> = this.copy(Root::class, Next::class, nextProperty)

//@Suppress("SuspiciousCallableReferenceInLambda")
//inline fun <reified Root : Any, reified Next : Any> KClass<Root>.atc(
//    noinline nextProperty: NextProperty<Root, Next>
//): CopyChain<Next, Root> = this.copy(Root::class, Next::class, nextProperty)

@Suppress("SuspiciousCallableReferenceInLambda")
inline fun <reified Root : Any> Root.copyAll(
    on: CopyChain<Root, Root>.(Root) -> Unit
): Root {
    return CopyChain(this, Root::class) { it }.assign(this)
}

@Suppress("SuspiciousCallableReferenceInLambda")
fun <Root : Any, Next : Any> Root.copy(
    rootClass: KClass<Root>,
    nextClass: KClass<Next>,
    nextProperty: NextProperty<Root, Next>
): CopyChain<Next, Root> = chainCopy(rootClass, nextClass, nextProperty) { previous -> previous }

//@Suppress("SuspiciousCallableReferenceInLambda")
//fun <Root : Any, Next : Any> KClass<Root>.atc(
//    rootClass: KClass<Root>,
//    nextClass: KClass<Next>,
//    nextProperty: NextProperty<Root, Next>
//): CopyChain<Next, Root> = chainCopy(this, nextClass, nextProperty) { previous -> previous }

class CopyChain<Previous : Any, Root : Any>(
    private val previousData: Previous,
    private val previousClass: KClass<Previous>,
    private val chain: (Previous) -> Root
) {

    @Suppress("SuspiciousCallableReferenceInLambda")
    inline infix fun <reified Next : Any> copy(noinline nextProperty: NextProperty<Previous, Next>) =
        copy(Next::class, nextProperty)

    @Suppress("SuspiciousCallableReferenceInLambda")
    fun <Next : Any> copy(nextClass: KClass<Next>, nextProperty: NextProperty<Previous, Next>): CopyChain<Next, Root> =
        previousData.chainCopy(previousClass, nextClass, nextProperty) { previous -> chain(previous) }

    infix fun assign(previous: Previous): Root = chain(previous)

    infix fun set(previous: Previous) {

    }

    @Suppress("SuspiciousCallableReferenceInLambda")
    infix fun copyAll(on: CopyChain<Previous, Root>.(Previous) -> Unit) {

    }
}

private fun <Previous : Any, Root : Any, Next : Any> Previous.chainCopy(
    previousClass: KClass<Previous>,
    nextClass: KClass<Next>,
    getNextProperty: NextProperty<Previous, Next>,
    chain: (Previous) -> Root
): CopyChain<Next, Root> {
    val nextProperty = getNextProperty()
    return CopyChain(nextProperty.get(), nextClass) {
        chain(copy(previousClass, nextProperty, it))
    }
}

fun <Data : Any, Property> Data.copy(
    dataClass: KClass<Data>,
    property: KProperty0<Property>,
    argument: Property
): Data {
    require(dataClass.isData) { "copy only works on data classes" }
    @Suppress("UNCHECKED_CAST")
    val copyFunction = dataClass.functions.first { it.name == "copy" } as KFunction<Data>
    val copyFunctionReceiver = copyFunction.parameters.first()
    val parameter = copyFunction.parameters.firstOrNull { it.name == property.name }
    requireNotNull(parameter) { "$dataClass does not contain $property" }
    return copyFunction.callBy(
        mapOf(
            copyFunctionReceiver to this,
            parameter to argument
        )
    )
}
