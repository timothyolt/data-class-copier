package tech.materialize.data

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch

//interface Subject<T> {
//    fun subscribe(subscriber: Subscriber<T>)
//}
//
//interface Publisher<T> : Subject<T> {
//    fun publish(subject: T)
//}
//
//interface Subscriber<T> {
//    fun update(subject: T)
//}
//
//class NaivePublisher<T> : Publisher<T> {
//    private val subscribers = mutableListOf<Subscriber<T>>()
//
//    override fun subscribe(subscriber: Subscriber<T>) {
//        subscribers += subscriber
//    }
//
//    override fun publish(subject: T) {
//        for (subscriber in subscribers) {
//            subscriber.update(subject)
//        }
//    }
//
//}
//
//class FlowPublisher<T>(private val scope: CoroutineScope) : Publisher<T> {
//    private val flow = MutableSharedFlow<T>()
//
//    override fun subscribe(subscriber: Subscriber<T>) {
//        scope.launch {
//            flow.collect(subscriber::update)
//        }
//    }
//
//    override fun publish(subject: T) {
//        scope.launch {
//            flow.tryEmit(subject)
//        }
//    }
//}
//
//open class Object(
//    private val getMessage: suspend () -> String,
//    private val scope: CoroutineScope
//) {
//    var message = ""
//        private set
//
//    init {
//        scope.launch {
//            message = getMessage()
//        }
//    }
//}
//
//class ObjectSubject(
//    private val publisher: Publisher<Object>
//) : Object, Subject<Object> by publisher {
//
//
//
//}